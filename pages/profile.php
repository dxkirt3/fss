
<?php 

    require("../scripts/connect.php");

    if(empty($_SESSION['user']))
    {
        header("Location: login.php");
    }
    else
    {
        $user_id = $_SESSION['user']['ID'];
        $login_counter = $_SESSION['logged_in'];
    }

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PROFILE | FSS MASTER SITE</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body class="" onload="page_load('<?php echo $_SESSION['user']['name'];?>', <?php echo $login_counter;?>);">

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="no image found" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $_SESSION['user']['name'];?></strong>

                            <?php 
                            
                                $role_query = mysqli_query($conn, "SELECT * FROM roles left join users on role_id = roles.id where users.id = '$user_id'");

                                $row_roles = mysqli_fetch_array($role_query);
                            
                            ?>

                             </span> <span class="text-muted text-xs block"><?php echo $row_roles['role'];?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.php">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="../scripts/logout.php">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php">Dashboard</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Account</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="account.php">My Account</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for profile..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to FSS Master Site</span>
                </li>
                <li class="dropdown">
                    <a id="notification" class="dropdown-toggle count-info animated" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-warning">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.php">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="../scripts/logout.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Profile</h2>
                    <ol class="breadcrumb">
                        <li>
                            <p>Profile</p>
                        </li>
                        <li class="active">
                            <strong>My Profile</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content animated">
                    <!--<h3 class="font-bold">This is page content</h3>-->
                    
                    
                <div class="row m-b-lg m-t-lg">
                    <div class="col-md-6">

                        <div class="profile-image">
                            <img src="img/a4.jpg" class="img-circle circle-border m-b-md" alt="No Image Found">
                        </div>

                        <?php 
                            
                            $role_query = mysqli_query($conn, "SELECT * FROM roles left join users on role_id = roles.id where users.id = '$user_id'");

                            $row_roles = mysqli_fetch_array($role_query);

                            $user_region_query = mysqli_query($conn, "SELECT region_name from users left join user_region on users.ID = user_region.user_id left join regions on regions.ID = user_region.region_id where users.ID = '$user_id'");

                            $user_details_query = mysqli_query($conn, "SELECT * from users left join user_details on users.ID = user_details.user_id where users.ID = '$user_id'");

                            $row_user_region = mysqli_fetch_array($user_region_query);
                            $row_user_details = mysqli_fetch_array($user_details_query);

                                    
                        ?>          

                        <div class="profile-info">
                            <div class="">
                                <div>
                                    <h2 class="no-margins">
                                        <?php echo $_SESSION['user']['name']?>
                                    </h2>

                                


                                    <h4><?php echo $row_roles['role'];?>  | <small><?php echo $row_user_region['region_name'];?></small></h4>
                                    <small>
                                        <?php echo $row_user_details['address'];?>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <table class="table small m-b-xs">
                            <tbody>
                            <tr>
                                <td>
                                    <strong>142</strong> Projects
                                </td>
                                <td>
                                    <strong>22</strong> Followers
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <strong>61</strong> Comments
                                </td>
                                <td>
                                    <strong>54</strong> Articles
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>154</strong> Tags
                                </td>
                                <td>
                                    <strong>32</strong> Friends
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <small>Sales in last 24h</small>
                        <h2 class="no-margins">206 480</h2>
                        <div id="sparkline1"></div>
                    </div>


                </div>
                <div class="row">

                    <div class="col-lg-3">

                        <div class="ibox">
                            <div class="ibox-content">
                                    <h3>About <?php echo $_SESSION['user']['name'];?></h3>

                                <p class="small">
                                    <?php 

                                        echo $row_user_details['about_me'];

                                    ?>
                                </p>

                                <p class="small font-bold">
                                    <span><i class="fa fa-phone text-navy"></i>

                                    <?php 

                                        echo $row_user_details['contact'];

                                    ?>

                                    </span>
                                </p>

                                <p class="small font-bold">
                                    <span><i class="fa fa-envelope text-info"></i>

                                    <?php 

                                        echo $row_user_details['email'];

                                    ?>

                                    </span>
                                </p>

                                <p class="small font-bold">
                                    <span><i class="fa fa-comment"></i>

                                    <?php 

                                        echo $row_user_details['shortname'];

                                    ?>

                                    </span>
                                </p>

                            </div>
                        </div>

                        <div class="ibox">
                            <div class="ibox-content">
                                <h3>Followers and friends</h3>
                                <p class="small">
                                    If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't
                                    anything embarrassing
                                </p>
                                <div class="user-friends">
                                    <a href=""><img alt="image" class="img-circle" src="img/a3.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a1.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a2.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a4.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a5.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a6.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a7.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a8.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a2.jpg"></a>
                                    <a href=""><img alt="image" class="img-circle" src="img/a1.jpg"></a>
                                </div>
                            </div>
                        </div>

                        <div class="ibox">
                            <div class="ibox-content">
                                <h3>Personal friends</h3>
                                <ul class="list-unstyled file-list">
                                    <li><a href=""><i class="fa fa-file"></i> Project_document.docx</a></li>
                                    <li><a href=""><i class="fa fa-file-picture-o"></i> Logo_zender_company.jpg</a></li>
                                    <li><a href=""><i class="fa fa-stack-exchange"></i> Email_from_Alex.mln</a></li>
                                    <li><a href=""><i class="fa fa-file"></i> Contract_20_11_2014.docx</a></li>
                                    <li><a href=""><i class="fa fa-file-powerpoint-o"></i> Presentation.pptx</a></li>
                                    <li><a href=""><i class="fa fa-file"></i> 10_08_2015.docx</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="ibox">
                            <div class="ibox-content">
                                <h3>Private message</h3>

                                <p class="small">
                                    Send private message to Alex Smith
                                </p>

                                <div class="form-group">
                                    <label>Subject</label>
                                    <input type="email" class="form-control" placeholder="Message subject">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control" placeholder="Your message" rows="3"></textarea>
                                </div>
                                <button class="btn btn-primary btn-block">Send</button>

                            </div>
                        </div>

                    </div>

                    <div class="col-lg-5">

                        <div class="social-feed-box">

                            <div class="pull-right social-action dropdown">
                                <button data-toggle="dropdown" class="dropdown-toggle btn-white">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu m-t-xs">
                                    <li><a href="#">Config</a></li>
                                </ul>
                            </div>
                            <div class="social-avatar">
                                <a href="" class="pull-left">
                                    <img alt="image" src="img/a1.jpg">
                                </a>
                                <div class="media-body">
                                    <a href="#">
                                        Andrew Williams
                                    </a>
                                    <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                </div>
                            </div>
                            <div class="social-body">
                                <p>
                                    Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                                    default model text, and a search for 'lorem ipsum' will uncover many web sites still
                                    in their infancy. Packages and web page editors now use Lorem Ipsum as their
                                    default model text.
                                </p>

                                <div class="btn-group">
                                    <button class="btn btn-white btn-xs"><i class="fa fa-thumbs-up"></i> Like this!</button>
                                    <button class="btn btn-white btn-xs"><i class="fa fa-comments"></i> Comment</button>
                                    <button class="btn btn-white btn-xs"><i class="fa fa-share"></i> Share</button>
                                </div>
                            </div>
                            <div class="social-footer">
                                <div class="social-comment">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a1.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Andrew Williams
                                        </a>
                                        Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words.
                                        <br/>
                                        <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 26 Like this!</a> -
                                        <small class="text-muted">12.06.2014</small>
                                    </div>
                                </div>

                                <div class="social-comment">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a2.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Andrew Williams
                                        </a>
                                        Making this the first true generator on the Internet. It uses a dictionary of.
                                        <br/>
                                        <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 11 Like this!</a> -
                                        <small class="text-muted">10.07.2014</small>
                                    </div>
                                </div>

                                <div class="social-comment">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a3.jpg">
                                    </a>
                                    <div class="media-body">
                                        <textarea class="form-control" placeholder="Write comment..."></textarea>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="social-feed-box">

                            <div class="pull-right social-action dropdown">
                                <button data-toggle="dropdown" class="dropdown-toggle btn-white">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu m-t-xs">
                                    <li><a href="#">Config</a></li>
                                </ul>
                            </div>
                            <div class="social-avatar">
                                <a href="" class="pull-left">
                                    <img alt="image" src="img/a6.jpg">
                                </a>
                                <div class="media-body">
                                    <a href="#">
                                        Andrew Williams
                                    </a>
                                    <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                </div>
                            </div>
                            <div class="social-body">
                                <p>
                                    Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                                    default model text, and a search for 'lorem ipsum' will uncover many web sites still
                                    in their infancy. Packages and web page editors now use Lorem Ipsum as their
                                    default model text.
                                </p>
                                <p>
                                    Lorem Ipsum as their
                                    default model text, and a search for 'lorem ipsum' will uncover many web sites still
                                    in their infancy. Packages and web page editors now use Lorem Ipsum as their
                                    default model text.
                                </p>
                                <img src="img/gallery/3.jpg" class="img-responsive">
                                <div class="btn-group">
                                    <button class="btn btn-white btn-xs"><i class="fa fa-thumbs-up"></i> Like this!</button>
                                    <button class="btn btn-white btn-xs"><i class="fa fa-comments"></i> Comment</button>
                                    <button class="btn btn-white btn-xs"><i class="fa fa-share"></i> Share</button>
                                </div>
                            </div>
                            <div class="social-footer">
                                <div class="social-comment">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a1.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Andrew Williams
                                        </a>
                                        Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words.
                                        <br/>
                                        <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 26 Like this!</a> -
                                        <small class="text-muted">12.06.2014</small>
                                    </div>
                                </div>

                                <div class="social-comment">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a2.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Andrew Williams
                                        </a>
                                        Making this the first true generator on the Internet. It uses a dictionary of.
                                        <br/>
                                        <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 11 Like this!</a> -
                                        <small class="text-muted">10.07.2014</small>
                                    </div>
                                </div>

                                <div class="social-comment">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a8.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Andrew Williams
                                        </a>
                                        Making this the first true generator on the Internet. It uses a dictionary of.
                                        <br/>
                                        <a href="#" class="small"><i class="fa fa-thumbs-up"></i> 11 Like this!</a> -
                                        <small class="text-muted">10.07.2014</small>
                                    </div>
                                </div>

                                <div class="social-comment">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a3.jpg">
                                    </a>
                                    <div class="media-body">
                                        <textarea class="form-control" placeholder="Write comment..."></textarea>
                                    </div>
                                </div>

                            </div>

                        </div>




                    </div>
                    <div class="col-lg-4 m-b-lg">
                        <div id="vertical-timeline" class="vertical-container light-timeline no-margins">
                            <div class="vertical-timeline-block">
                                <div class="vertical-timeline-icon navy-bg">
                                    <i class="fa fa-briefcase"></i>
                                </div>

                                <div class="vertical-timeline-content">
                                    <h2>Meeting</h2>
                                    <p>Conference on the sales results for the previous year. Monica please examine sales trends in marketing and products. Below please find the current status of the sale.
                                    </p>
                                    <a href="#" class="btn btn-sm btn-primary"> More info</a>
                                        <span class="vertical-date">
                                            Today <br>
                                            <small>Dec 24</small>
                                        </span>
                                </div>
                            </div>

                            <div class="vertical-timeline-block">
                                <div class="vertical-timeline-icon blue-bg">
                                    <i class="fa fa-file-text"></i>
                                </div>

                                <div class="vertical-timeline-content">
                                    <h2>Send documents to Mike</h2>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                                    <a href="#" class="btn btn-sm btn-success"> Download document </a>
                                        <span class="vertical-date">
                                            Today <br>
                                            <small>Dec 24</small>
                                        </span>
                                </div>
                            </div>

                            <div class="vertical-timeline-block">
                                <div class="vertical-timeline-icon lazur-bg">
                                    <i class="fa fa-coffee"></i>
                                </div>

                                <div class="vertical-timeline-content">
                                    <h2>Coffee Break</h2>
                                    <p>Go to shop and find some products. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's. </p>
                                    <a href="#" class="btn btn-sm btn-info">Read more</a>
                                    <span class="vertical-date"> Yesterday <br><small>Dec 23</small></span>
                                </div>
                            </div>

                            <div class="vertical-timeline-block">
                                <div class="vertical-timeline-icon yellow-bg">
                                    <i class="fa fa-phone"></i>
                                </div>

                                <div class="vertical-timeline-content">
                                    <h2>Phone with Jeronimo</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                                    <span class="vertical-date">Yesterday <br><small>Dec 23</small></span>
                                </div>
                            </div>

                            <div class="vertical-timeline-block">
                                <div class="vertical-timeline-icon navy-bg">
                                    <i class="fa fa-comments"></i>
                                </div>

                                <div class="vertical-timeline-content">
                                    <h2>Chat with Monica and Sandra</h2>
                                    <p>Web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). </p>
                                    <span class="vertical-date">Yesterday <br><small>Dec 23</small></span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="footer">
                <div class="pull-right">
                    All<strong>Rights</strong> Reserve.
                </div>
                <div>
                    <strong>Copyright</strong> KG &copy; 2017
                </div>
            </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Toastr script -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>  

    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>


    <script type="text/javascript">

        function page_load(name, counter)
        {
            var time_interval = setInterval(function(){
                $('#notification').addClass('animated');
                $('#notification').addClass('tada');

                setTimeout(function() {
                    $('#notification').removeClass('tada');
                }, 1000);

                
            }, 2000);

            if(counter == 1)
            {
                swal({
                    title: "Good Day",
                    text: "Hello " + name +"!",
                    type: "success"
                });
            }
        }

    </script>

    <?php $_SESSION['logged_in'] = 0;?>

</body>

</html>

<!-- 

    if(empty($_GET['uid']))
    {
        Header("Location: 404.php");
    }
    else
    {
        $uid = $_GET['uid'];
        $uid = decryptIt($uid);
    }

    function decryptIt( $q ) {
        $cryptKey  = 'uid';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }


-->