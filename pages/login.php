<?php 

    require("../scripts/connect.php");

    // LOCAL Variable declration
    $login_status = 0;


    // Check GET
    if (!empty($_GET))
    {
        // set counter value to login_status
        $login_status = $_GET['counter'];
    }

    if(!empty($_SESSION['user']))
    {
        header("Location: index.php");
    }

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>FSS | Login</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</head>

<body class="gray-bg" onload="page_load(<?php echo $login_status;?>);">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">FSS</h1>

            </div>
            <h3>WELCOME TO FSS MASTER SITE</h3>
            <p>Contents will be added on this text...
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Login in. To see it in action.</p>
            <form id="login-form" class="m-t" role="form" action="../scripts/login.php" method="POST" >
                <div class="form-group">
                    <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="button" class="btn btn-primary block full-width m-b dim" onclick="login()">Login</button>

                <a href="#"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="request_login.php">Send Request to Admin</a>
            </form>
            <p class="m-t"> <small>&copy; 2017</small> </p>
        </div>
    </div>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>

    <!-- Toastr script -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>  

    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- LOCAL SCRIPTS -->
    <script type"text/javascript">

        function page_load(counter)
        {
            //alert(counter);
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    positionClass: 'toast-top-center',
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                if(counter == 3 || counter == 1)
                {
                    toastr.error('Invalid username or password', 'Error');
                    window.history.replaceState(null, null, window.location.pathname);
                }
                if(counter == 2)
                {
                    toastr.success('Logout Successfully', 'Success');
                    window.history.replaceState(null, null, window.location.pathname);
                }
                

            }, 1300);
        }

        function login()
        {
            var username = document.getElementById("username").value;
            //alert(username);
            swal({
                title: "Logging in As",
                text: username,
                type: "success"
            });

            setTimeout(function() {
                document.getElementById("login-form").submit();
            }, 2000);
            
        }
        
    </script>

</body>

</html>
