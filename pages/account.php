
<?php 

    require("../scripts/connect.php");

    if(empty($_SESSION['user']))
    {
        header("Location: login.php");
    }
    else
    {
        $user_id = $_SESSION['user']['ID'];
        $login_counter = $_SESSION['logged_in'];
    }

?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>My Account | FSS MASTER SITE</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="../css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <!-- Ladda style -->
    <link href="../css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


    <style>

        .wizard > .content > .body  position: relative; }

    </style>

</head>

<body>

    <div id="wrapper">

     <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="no image found" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $_SESSION['user']['name'];?></strong>

                            <?php 
                            
                                $role_query = mysqli_query($conn, "SELECT * FROM roles left join users on role_id = roles.id where users.id = '$user_id'");

                                $row_roles = mysqli_fetch_array($role_query);
                            
                            ?>

                             </span> <span class="text-muted text-xs block"><?php echo $row_roles['role'];?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="profile.php">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="../scripts/logout.php">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php">Dashboard</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Account</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="account.php">My Account</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for profile..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to FSS Master Site</span>
                </li>
                <li class="dropdown">
                    <a id="notification" class="dropdown-toggle count-info animated" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-warning">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="../scripts/logout.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>My Account</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a>Account</a>
                        </li>
                        <li class="active">
                            <strong>My Account</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row animated" id="account-jumbotron">
                <div class="col-lg-12">
                    <div class="jumbotron">
                        <h1>Welcome to My Account</h1>
                        <p>In this section you can update your account details.</p>
                        <button id="hide-jumbo" type="button" class="btn btn-primary btn-lg dim" >Hide this Section</a>
                        
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox collapsed ">
                        <div class="ibox-title collapse-link">
                            <h5>Login</h5>
                            <div class="ibox-tools">
                                <a>
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="username-update" action="#" method="post">
                                        <label>Username</label>
                                        <div class="input-group">
                                            <input id="userName" name="username" type="text" class="form-control required" value="<?php echo $_SESSION['user']['username'];?>">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" id="btn-username-update" data-style="slide-down">Update</button>
                                            </span>
                                        </div>
                                    </form>
                                    <hr/>
                                    <form id="username-update" action="#" method="post">
                                        <label>Old Password</label>
                                        <input name="oldpPassword" type="password" class="form-control required">
                                        <label>New Password</label>
                                        <input name="password" type="password" class="form-control required">
                                        <label>Confirm Password</label>
                                        <input name="confirmPassword" type="password" class="form-control required" >
                                        <br/>
                                        <button type="button" class="btn btn-primary btn-block" id="btn-username-update" data-style="slide-down">Update</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox collapsed ">
                        <div class="ibox-title collapse-link">
                            <h5>Personal Details</h5>
                            <div class="ibox-tools">
                                <a>
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form id="form" action="#" >
                                <fieldset>
                                    <h2>User Information</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input name="name" type="text" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>shortname</label>
                                                <input id="password" name="password" type="text" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Gender</label>
                                                <input id="gender" name="confirm" type="text" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <button id="hide-jumbo" type="button" class="btn btn-block btn-success btn-lg" >Update</button>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="footer">
                <div class="pull-right">
                    All<strong>Rights</strong> Reserve.
                </div>
                <div>
                    <strong>Copyright</strong> KG &copy; 2017
                </div>
            </div>

        </div>
    </div>



    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>

    <!-- Steps -->
    <script src="../js/plugins/staps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="../js/plugins/validate/jquery.validate.min.js"></script>

    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>


    <script>
        $(document).ready(function(){


            $( "#hide-jumbo" ).click(function() {
              $("#account-jumbotron").addClass("bounceOutUp");
              setTimeout(function() {
                    $("#account-jumbotron").hide("slow");
                }, 500);
              
            });

            $("#btn-username-update").click(function(){
                swal({
                    title: "Logging in As",
                    text: "asdas",
                    type: "success"
                }, function(){swal("deleted", "adadwadas", "success")});
            });
            
            var time_interval = setInterval(function(){
                $('#notification').addClass('animated');
                $('#notification').addClass('tada');

                setTimeout(function() {
                    $('#notification').removeClass('tada');
                }, 1000);

                
            }, 2000);
            

       });
    </script>

</body>

</html>
