
<?php 

    require("../scripts/connect.php");

    if(empty($_SESSION['user']))
    {
        header("Location: login.php");
    }
    else
    {
        $user_id = $_SESSION['user']['ID'];
        $login_counter = $_SESSION['logged_in'];
    }


?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>HOME | FSS MASTER SITE</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body class="" onload="page_load('<?php echo $_SESSION['user']['name'];?>', <?php echo $login_counter;?>);">

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="no image found" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $_SESSION['user']['name'];?></strong>

                            <?php 
                            
                                $role_query = mysqli_query($conn, "SELECT * FROM roles left join users on role_id = roles.id where users.id = '$user_id'");

                                $row_roles = mysqli_fetch_array($role_query);
                            
                            ?>

                             </span> <span class="text-muted text-xs block"><?php echo $row_roles['role'];?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">

                            <li><a href="profile.php">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="../scripts/logout.php">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php">Dashboard</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Account</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="account.php">My Account</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for profile..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to FSS Master Site</span>
                </li>
                <li class="dropdown">
                    <a id="notification" class="dropdown-toggle count-info animated" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-warning">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="../scripts/logout.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <p>Home</p>
                        </li>
                        <li class="active">
                            <strong>Dashboard</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary">This is action area</a>
                    </div>
                </div>
            </div>

            <div class="wrapper wrapper-content animated">
                    <!--<h3 class="font-bold">This is page content</h3>-->
                    <div class="row">
                    <?php 
                    
                        $region_query = mysqli_query($conn, "SELECT * FROM regions");

                        while($row_region = mysqli_fetch_array($region_query))
                        {
                    ?>
                    
                        <div class="col-lg-4">
                            <div class="widget-head-color-box navy-bg p-lg text-center">
                                <div class="m-b-md">
                                <small>REGION</small>
                                <h2 class="font-bold no-margins">
                                    <?php echo $row_region['region_name'];?>
                                </h2>
                                </div>
                                <!--<img src="img/a4.jpg" class="img-circle circle-border m-b-md" alt="profile">-->
                                <div>
                                    <!--<span>100 Tweets</span> | -->
                                </div>
                            </div>
                            <div class="widget-text-box">
                                <h4 class="media-heading">Description</h4>
                                <p><?php echo $row_region['description'];?></p>
                                <div class="text-right">
                                    <a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> Like </a>
                                    <a class="btn btn-xs btn-primary"><i class="fa fa-heart"></i> Love</a>
                                </div>
                            </div>
                    </div>
                    <?php } ?>
                    </div>
            </div>

            <div class="footer">
                <div class="pull-right">
                    All<strong>Rights</strong> Reserve.
                </div>
                <div>
                    <strong>Copyright</strong> KG &copy; 2017
                </div>
            </div>

        </div>
        </div>

    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Toastr script -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>  

    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>


    <script type="text/javascript">

        function page_load(name, counter)
        {
            var time_interval = setInterval(function(){
                $('#notification').addClass('animated');
                $('#notification').addClass('tada');

                setTimeout(function() {
                    $('#notification').removeClass('tada');
                }, 1000);

                
            }, 2000);

            if(counter == 1)
            {
                swal({
                    title: "Good Day",
                    text: "Hello " + name +"!",
                    type: "success"
                });
            }
        }

    </script>

    <?php $_SESSION['logged_in'] = 0;?>

</body>

</html>


<!-- 


                            $encrypted_id = encryptIt($_SESSION['user']['ID']);

                            function encryptIt( $q ) {
                                $cryptKey  = 'uid';
                                $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
                                return( $qEncoded );
                            }
-->