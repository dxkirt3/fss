<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Page Not Found | FSS MASTER SITE</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold">Page Not Found</h3>

        <div class="error-desc">
            Sorry the page you're looking for might be down or under maintenance, please try doing what you did to come up here again if it works. If everything seems to be not working as expected then please contact your administrator. <br/><br/>
            <button type="button" class="btn btn-primary block full-width m-b dim" onclick="redirect();">Go Back to Previous Page</button>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script type="text/javascript">
        function redirect()
        {
            window.history.back();
        }
    </script>

</body>

</html>
