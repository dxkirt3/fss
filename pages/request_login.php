<?php 


?>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>FSS | Access Request</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="../js/jquery-2.1.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</head>

<body class="gray-bg">

    <div class="passwordBox animated fadeInDown">
        <div class="row">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Request Access</h2>

                    <p>
                        Enter your name and your request access message.
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" id="access_request" action="index.html" method="POST">
                                <div class="form-group">
                                    <input id="name" type="name" name="name" class="form-control" placeholder="Name" required="">
                                </div>

                                <div class="form-group">
                                    <textarea name="message" class="form-control" placeholder="Message" required=""></textarea>
                                </div>

                                <button type="button" class="btn btn-primary block full-width m-b dim" onclick="send_request()">Submit Request</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-5">
            <a class="btn btn-sm btn-white btn-block dim" href="login.php"><i class="fa fa-arrow-circle-left"></i> Back to Login</a>
            </div>
            <div class="col-md-7 text-right">
               <small>© 2017</small>
            </div>
        </div>
    </div>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>

    <!-- Toastr script -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>  

    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- LOCAL SCRIPTS -->
    <script type"text/javascript">

        function page_load(counter)
        {
            //alert(counter);
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    positionClass: 'toast-top-center',
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                if(counter == 3 || counter == 1)
                {
                    toastr.error('Invalid username or password', 'Error');
                }
                if(counter == 2)
                {
                    toastr.success('Login successful', 'Success');
                }
                

            }, 1300);
        }

        function send_request()
        {
            var name = document.getElementById("name").value;
            //alert(username);
            var message = "Request for " + name + " is being submitted";
            swal({
                title: "Submitting Request!",
                text: message,
                type: "success"
            });

            setTimeout(function() {
                document.getElementById("access_request").submit();
            }, 2000);
            
        }
        
    </script>

</body>

</html>
