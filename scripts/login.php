<?php 

    require("connect.php");
    require("passwordHash.php");

    // POST Variable declaration
    $username = $_POST['username'];
    $password = $_POST['password'];
    // POST Variable declaration


    // local Variable declaration
    $query_counter = 0;                                                                         // default/initial value set to 0
    $keyid = 0;                                                                                 // declarate keyid variable
    $key = "";                                                                                  // declare key variable
    $hash = "";                                                                                 // hashing variable to be encrypted
    $encryptedPassword = "";                                                                    // declare variable for encrypted password            

    // query in getting the users
    $getuser_query = mysqli_query($conn, "SELECT * FROM users where username = '$username'");

    // fetch the query results in to an array
    while($row = mysqli_fetch_array($getuser_query))
    {
        $encryptedPassword = $row['password'];                                                  // Gets the encrypted password from the database
        $keyid = substr($encryptedPassword, 0, 1);                                              // Gets the keyId from the encrypted password
        $encryptedPassword = substr($encryptedPassword, 1);                                     // removes the keyid from the encrypted password
        
        // query in gettign the id
        $getKey_query = mysqli_query($conn, "SELECT * FROM authentication where ID = '$keyid'");

        $row_key = mysqli_fetch_array($getKey_query);                                           // fetch the query result

        $key = $row_key['auth_key'];                                                            // sets the key value from the db to key variable

        $hash = $password . $key;                                                               // sets the hash value

        if(validate_password($hash, $encryptedPassword))                                        // decrypt password and check if password is correct
        {
            $query_counter = 1;                                                                 // if password was correct then redirect inside the site
            $_SESSION['user'] = $row;
            $_SESSION['logged_in'] = 1;
            header("Location: ../pages/index.php");
            exit();
        }
        else
        {
            $query_counter = 2;                                                                // if password was incorrect then redirect back to login 
            header("Location: ../pages/login.php?counter=3");
            exit();
        }
        
    }
    
    if($query_counter == 0 )                                                                   // if counter is equal to 0 it means no account found
    {
        header("Location: ../pages/login.php?counter=1");
        exit();
    }

    

?>